JAVA (1.6) SDK
==============

- version: 1.3.0
- compatible with: API version 1.3
- __Minimum__ Java version: 1.6

Required Packages:
------------------

- [Legion of the Bouncy Castle 1.46](https://www.bouncycastle.org/java.html) ([zip](https://bitbucket.org/matchmove/api-vcard-sdk-java/downloads/bcprov-jdk16-1.46.jar.zip))
- [Apache Common Codec 1.10](http://commons.apache.org/proper/commons-codec/download_codec.cgi) ([zip](https://bitbucket.org/matchmove/api-vcard-sdk-java/downloads/commons-codec-1.10.jar.zip))

Dependencies:
-------------
- [JSONObject](https://github.com/douglascrockford/JSON-java)


Installation:
-------------

1. Install this repository to `src/com/matchmove/mmpay/api` which will yield a package name `com.matchmove.mmpay.api`.
    
        git clone https://bitbucket.org/matchmove/api-vcard-sdk-java src/com/matchmove/mmpay/api

    or as a submodule,
    
        git submodule add https://bitbucket.org/matchmove/api-vcard-sdk-java src/com/matchmove/mmpay/api

2. Install __JSONObject__. The package should be `org.json`.

        git clone https://github.com/douglascrockford/JSON-java src/org/json

    or as a submodule,
    
        git submodule add https://github.com/douglascrockford/JSON-java src/org/json

3. Lastly, Add __Bouncy Castle__ as a _security provider_.

        :::java
        Security.addProvider(new BouncyCastleProvider());

Usage:
------



### Initializing a `Connection` object

    :::java
    Connection wallet = new Connection(host, consumerKey, consumerSecret);
    
### Consuming public resources

    :::java
    Connection wallet = new Connection(host, consumerKey, consumerSecret);
    
    // List all card types.
    JSONObject cardTypes = wallet.consume("users/wallets/cards/types");
    
    // Register a user.
    Map<String, String> userData = new HashMap<String, String>();
    
    userData.put("email", email);
    userData.put("password", password);
    userData.put("first_name", "John");
    userData.put("last_name", "Doe");
    userData.put("mobile_country_code", "65");
    userData.put("mobile", "98765432");
    
    JSONObject user = wallet.consume("users", HttpRequest.METHOD_POST, userData);
    
### "Logging in" / User authentication

    :::java
    Connection wallet = new Connection(host, consumerKey, consumerSecret);
    wallet.authenticate(user, password);
    
###  Session handling

@ main.java

    :::java
    Connection wallet = new Connection(host, consumerKey, consumerSecret);
    wallet.setSession(new Session());
    
    if (!wallet.authenticate(user)) {
    
        // Authentication session cannot be found
        wallet.authenticate(user, password);
    
    }
        
    // User is already logged in
    
@  session.java

    :::java
    import com.matchmove.mmpay.api.ConnectionSession;
    
    public class Session implements ConnectionSession {

	
	    @Override
	    public boolean set(String key, String value) {
		    return this.set(key, value, Session.expires);
	    }
	
	    @Override
	    public boolean set(String key, String value, long expires) {
	        // ...
		    return result;
	    }

	    @Override
	    public String get(String key) {
		    // ...		
		    return expires <= OAuth.timestamp() ? null: data.get("value");
	    }
    }
 
### Consume Authenticated resources

    :::java
    Connection wallet = new Connection(host, consumerKey, consumerSecret);
    wallet.authenticate(user, password);
    
    // Get user details
    JSONObject user = wallet.consume("users")
    
    // Update user's details
	Map<String, String> userData = new HashMap<String, String>();
	
	userData.put("title", "Mr");
	userData.put("gender", "male");
	userData.put("id_number", "s1234567890");
	userData.put("id_type", "nric");
	userData.put("country_of_issue", "Singapore");
			
    JSONObject userUpdated = wallet.consume("users", HttpRequest.METHOD_PUT, userData);
    
### Handling exceptions

    :::java
    Connection wallet = new Connection(host, consumerKey, consumerSecret);
    
    try {
        if (!wallet.authenticate(user)) {
            wallet.authenticate(user, password);
        }
    } catch (ResourceException e) {
        if (403 == e.getCode()) {
            System.out.println("Invalid username/password.");
        }
        
        // Or search for code in string,
        // e.getMessage().contains("user_crendentials_invalid");
    }
    
More Examples:
--------------

- [Demo.java](https://bitbucket.org/snippets/matchmove/kLzX)
- [Session.java](https://bitbucket.org/snippets/matchmove/KE96)

Issues
------

[Any inquiries, issues or feedbacks?](https://bitbucket.org/matchmove/api-vcard-sdk-java/issues)